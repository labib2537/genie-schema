<?php

namespace Genie\Schematojson\Schema;

use Illuminate\Support\ServiceProvider;

class SchemaServiceProvider extends ServiceProvider
{
    public function boot()
    {
    $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
    $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
    $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    $this->loadViewsFrom(__DIR__ . '/resources/views', 'schema');

    $this->commands([
        ##InstallationCommandClass##
    ]);
    
    
\Illuminate\Support\Facades\Blade::component('forms.select-storagetype', \Genie\Schematojson\Schema\App\View\Components\Forms\SelectStoragetype::class);

\Illuminate\Support\Facades\Blade::component('forms.select-storageconnection', \Genie\Schematojson\Schema\App\View\Components\Forms\SelectStorageconnection::class);
##||ANOTHERCOMPONENT||##
    
    }
    public function register()
    { }
}