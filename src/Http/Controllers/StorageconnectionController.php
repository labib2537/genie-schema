<?php

namespace Genie\Schematojson\Schema\Http\Controllers;

use Genie\Schematojson\Schema\App\SourceGoogleSpreadsheet;
use Genie\Schematojson\Schema\App\SourceOracle;
use Genie\Schematojson\Schema\App\SourceMySql;

use Genie\Schematojson\Schema\Http\Requests\StorageconnectionRequest;
use Genie\Schematojson\Schema\Models\Storageconnection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;



class StorageconnectionController extends StorageconnectionBaseController
{
    public function connect($uuid)
    {
        
        $storageconnection = Storageconnection::where('uuid', '=', $uuid)->first();
        $datasource = $storageconnection->storagetype_title;
        // dd($datasource);
        $this->prepareConnection($storageconnection);
        // dd($datasource);
        if(is_null($datasource))
        {
            echo 'data source not found';
        }
        if($datasource=='Google-Sheet')
        {
           return (new SourceGoogleSpreadsheet())->connect($storageconnection->title, $uuid);
        }

        if($datasource=='Oracle')
        {
           return (new SourceOracle())->connect($storageconnection->title, $uuid);
        }

        if($datasource=='MySQL')
        {
           return (new SourceMySql())->connect($storageconnection->title, $uuid);
        }
    }

    private function prepareConnection($storageconnection)
    {
        if(strtolower($storageconnection->storagetype_title)==='mysql'){
            $connection = [
                'driver' => strtolower($storageconnection->storagetype_title),
                'host' => $storageconnection->target,
                'port' => '3306',
                'database' => $storageconnection->storage,
                'username' => $storageconnection->client,
                'password' => $storageconnection->secret===null ? '':$storageconnection->secret,
            ];

        }
        if(strtolower($storageconnection->storagetype_title)==='oracle' ||
        strtolower($storageconnection->storagetype_title)==='google-sheet'){
            $connection = [
                'driver' => strtolower($storageconnection->storagetype_title),
                'host' => $storageconnection->target,
                'port' => '1521',
                'database' => $storageconnection->storage,
                'username' => $storageconnection->client,
                'password' => $storageconnection->secret===null ? '':$storageconnection->secret,
            ];
        }
            
        
        // dd($connection);
       
        \Config::set('database.connections.'. $storageconnection->title, $connection);
        // dd(\Config::get('database.connections'));
            
        
    }

    public function store(StorageconnectionRequest $request)
    {
        // dd($request->all());
        try {
            $storageconnection = Storageconnection::create(['uuid'=> (string)Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('storageconnections.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

   

    // public function jsonStructure2($connection, $uuid){
    //     $storageconnection = Storageconnection::where('uuid', '=', $uuid)->first();
    //     $datasource = $storageconnection->storagetype_title;
    //     $this->prepareConnection($storageconnection);

    //     $schema = DB::connection($connection)->getDoctrineSchemaManager();
    //     $tables = $schema->listTableNames();
    //     // dd($tables);
    //     $columns = [];
    //     foreach($tables as $key => $table){
    //         $columns[] =$schema->listTableColumns($tables[$key]);
    //     }
    //     // dd($tables[0]);
    //     $columns = $schema->listTableColumns($tables[0]);
    //     $value = $key;
    //     $dbalDataType = $this->variableTypes();        
    
    //     // $schema = DB::connection()->getDoctrineSchemaManager();
    //     $connectionName = $connection;

    //     // $engineName = config("database.connections.$connectionName.database");
    //     // $sql = "SELECT value FROM nls_database_parameters WHERE parameter = 'NLS_CHARACTERSET'";

    //     // $result = DB::connection($connectionName)->select($sql);

    //     // if (!empty($result)) {
    //     //     $charsetName = $result[0]->value;
    //     // } else {
    //     //     $charsetName = null;
    //     // }

    //     // $tables = $schema->listTableNames();
    //     // dd($tables);
    //     // $tableName = $tables[$key];    
       
        
         
    //     $columns = $schema->listTableColumns($tables[$key]);
    //     $jsonData = [];
    //      foreach($columns as $column){
    //         $attrtibutes = $column->toArray();
    //         $columnNames[$key] = $column->getType();            
    //         $jsonData[$attrtibutes['name']] = [                
    //             'dataType' => $dbalDataType[get_class($columnNames[$key])],
    //             'length' => $attrtibutes['length'] === 'NULL' ? null : $attrtibutes['length'],
    //             'precision' => $attrtibutes['precision'],
    //             'unsigned' => $attrtibutes['unsigned'] === 'TRUE',
    //             'notnull' => $attrtibutes['notnull'] === 'TRUE',
    //             'autoincrement' => $attrtibutes['autoincrement'],
    //             'columnDefinition' => $attrtibutes['columnDefinition'],
    //             'default' => $attrtibutes['default'] === 'NULL' ? null : $attrtibutes['default'],
    //             'comment' => $attrtibutes['comment'] === 'NULL' ? null : $attrtibutes['comment']
    //         ];
    //        }

    //     $jsonStructure = [
    //         "connection" => $connectionName,
    //         "tableName" =>  "table",
    //         "options" => [
    //             "engine" =>  "ngineName",
    //             "charset" => "charset",
    //         ],
    //         "columns" => $jsonData,
    //     ];    
    //     $response = response()->json($jsonStructure);
    //     $jsonData = $response->getData();
    //     return view('schema::structures.json', compact('response','value','jsonData'));
        
    // }

   
}
