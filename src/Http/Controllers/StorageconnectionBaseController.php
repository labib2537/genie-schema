<?php

namespace Genie\Schematojson\Schema\Http\Controllers;

use App\Http\Controllers\Controller;
use Genie\Schematojson\Schema\Http\Requests\StorageconnectionRequest;
use Genie\Schematojson\Schema\Models\Storageconnection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
//use another classes

class StorageconnectionBaseController extends Controller
{

    public static $visiblePermissions = [
        'index' => 'List',
        'create' => 'Create Form',
        'store' => 'Save',
        'show' => 'Details',
        'edit' => 'Edit Form',
        'update' => 'Update',
        'destroy' => 'Delete'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('schema::storageconnections.index', [
                        'storageconnections' => Storageconnection::latest()->get()
                     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('schema::storageconnections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorageconnectionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorageconnectionRequest $request)
    {
        // dd($request->all());
        try {
            $storageconnection = Storageconnection::create(['uuid'=> Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('storageconnections.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Storageconnection  $storageconnection
     * @return \Illuminate\Http\Response
     */
    public function show(Storageconnection $storageconnection)
    {
        return view('schema::storageconnections.show', compact('storageconnection'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Storageconnection  $storageconnection
     * @return \Illuminate\Http\Response
     */
    public function edit(Storageconnection $storageconnection)
    {
        return view('schema::storageconnections.edit', compact('storageconnection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StorageconnectionRequest  $request
     * @param  \App\Models\Storageconnection  $storageconnection
     * @return \Illuminate\Http\Response
     */
    public function update(StorageconnectionRequest $request, Storageconnection $storageconnection)
    {
        try {
            $storageconnection->update($request->all());
            
            //handle relationship update
            return redirect()->route('storageconnections.index')
                ->withSuccess(__('Successfully Updated'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Storageconnection  $storageconnection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Storageconnection $storageconnection)
    {
        try {
            $storageconnection->delete();

            return redirect()->route('storageconnections.index')
                ->withSuccess(__('Successfully Deleted'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

//another methods
}
