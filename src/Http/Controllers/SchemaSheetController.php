<?php

namespace Genie\Schematojson\Schema\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;

use Revolution\Google\Sheets\Facades\Sheets;
use Illuminate\Http\Request;
use App\Models\SchemaSheet;
use Illuminate\Support\Str;

class SchemaSheetController extends Controller
{

    public function input()
    {
        return view('schema::connectsheets.inputSpreadsheet');
    }

    public function selectTable(Request $req)
    {
        $spreadsheet_id = $req->spreadSheet;
        $tableData = null;
        $errorMessage = null;

        if ($spreadsheet_id) {
            try {
                $tableData = Sheets::spreadsheet($spreadsheet_id)->sheetList();
            } catch (\Exception $e) {
                $errorMessage = "Error: " . $e->getMessage(); // Set an error message
            }
        } else {
            $errorMessage = "Spreadsheet ID is not provided."; // Set an error message for missing ID
        }

        return view('schema::connect.selectTable', compact('spreadsheet_id', 'tableData', 'errorMessage'));
    }

    public function transformExcelToJson($spreadSheet, $sheet)
    {
        $spreadsheet_id = $spreadSheet;
        $sheet_identifier = $sheet;
        // $data = Sheets::spreadsheet($spreadsheet_id)->sheetList();  

        $data = Sheets::spreadsheet($spreadsheet_id)->sheet($sheet_identifier)->get();

        $dataArray = $data->toArray();
        $structureRow = array_shift($dataArray);
        $strData = array_shift($dataArray);

        $tableName = $strData[0];
        $connection = $strData[1];
        $engine = $strData[2];
        $charset = $strData[3];
        $package_id = $strData[4];

        array_shift($dataArray);  // removing empty row
        $header = array_shift($dataArray); // Extract the column defination labels from the first row   

        $jsonData = [];

        foreach ($dataArray as $key => $row) {
            $columnData = array_combine($header, $row);

            $columnName = $columnData['columns'];

            unset($columnData['columns']);

            // transform the data for the current column
            $column = [
                'dataType' => $columnData['dataType'],
                'length' => $columnData['length'] === 'NULL' ? null : $columnData['length'],
                'isPrimaryKey' => $columnData['isPrimaryKey'] === 'TRUE',
                'miscellaneous' => [
                    'unsigned' => $columnData['unsigned'] === 'TRUE',
                    'nullable' => $columnData['nullable'] === 'TRUE',
                    'default' => $columnData['default'] === 'NULL' ? null : $columnData['default'],
                    'comment' => $columnData['comment'] === 'NULL' ? null : $columnData['comment'],
                ],
            ];

            // add the column to the JSON data
            $jsonData[$columnName] = $column;
        }

        // create the final JSON structure
        $jsonStructure = [
            "connection" => $connection,
            "tableName" => $tableName,
            "options" => [
                "engine" => $engine,
                "charset" => $charset,
            ],
            "columns" => $jsonData,
        ];

        $response = response()->json($jsonStructure);
        $response->header('package-id', $package_id);
        return $response;
    }



    public function create(Request $req)
    {
        $spreadSheet = $req->spreadSheet;
        $sheet = $req->sheet;
        // dd($spreadSheet, $sheet);
        $jsonResponse = $this->transformExcelToJson($spreadSheet, $sheet);
        $jsonData = $jsonResponse->getData();
        // dd($jsonResponse->headers->get('package-id'));
        return view('schema::connect.create', compact('jsonResponse', 'jsonData'));
    }


    public function store(Request $req)
    {
        dd($req->all());

        $schema = new SchemaSheet;
        $schema->uuid = Str::uuid();
        $schema->package_id = $req->pkg_id;
        $schema->entity = $req->entity;
        $schema->connection_name = $req->conn_name;
        $schema->json_migration = $req->json_migration;
        $schema->save();
        return redirect()->route('inputSpreadSheet');
    }

    // to store into excel from oracle

    public function inputSpreadsheetForStore(Request $request)
    {
        $json = $request->json;
        // dd($json);

        return view('schema::connect.input-spreadsheet-oracle', compact('json'));
    }

    public function selectSheetForStore(Request $req)
    {
        $spreadsheet_id = $req->spreadSheet;
        $json = $req->json;
        $tableData = null;
        $errorMessage = null;

        if ($spreadsheet_id) {
            try {
                $tableData = Sheets::spreadsheet($spreadsheet_id)->sheetList();
            } catch (\Exception $e) {
                $errorMessage = "Error: " . $e->getMessage(); // Set an error message
            }
        } else {
            $errorMessage = "Spreadsheet ID is not provided."; // Set an error message for missing ID
        }

        return view('schema::connect.select-table-oracle', compact('spreadsheet_id', 'tableData', 'errorMessage', 'json'));
    }

    public function storeExcel(Request $request)
    {


        $spreadsheet_id = $request->spreadSheet;
        $sheet_identifier = $request->sheet;
        $json = $request->json;

        $data = json_decode($json, true);
        // dd($jsonArray);
        // Create the Excel sheet data array
        $excelData = [];
        $excelData[] = [
            'tableName',
            'connection',
            'engine',
            'charset',
            'package_id',
        ];

        $excelData[] = [
            $data['tableName'],
            $data['connection'],
            $data['options']['engine'],
            $data['options']['charset'],
            5, // You can set the package_id value as needed
        ];

        $excelData[] = []; // Add an empty row

        $excelData[] = [
            'columns',
            'dataType',
            'length',
            'isPrimaryKey',
            'unsigned',
            'nullable',
            'default',
            'comment',
        ];

        foreach ($data['columns'] as $columnName => $columnData) {
            $excelData[] = [
                $columnName,
                $columnData['dataType'],
                $columnData['length'] ?? 'NULL',
                $columnData['isPrimaryKey'] ? 'TRUE' : 'FALSE',
                $columnData['miscellaneous']['unsigned'] ? 'TRUE' : 'FALSE',
                $columnData['miscellaneous']['nullable'] ? 'TRUE' : 'FALSE',
                $columnData['miscellaneous']['default'] ?? 'NULL',
                $columnData['miscellaneous']['comment'] ?? 'NULL',
            ];
        }
        // dd($excelData);
        $sheet = Sheets::spreadsheet($spreadsheet_id)->sheet($sheet_identifier)->clear();
        $sheet = Sheets::spreadsheet($spreadsheet_id)->sheet($sheet_identifier)->append($excelData);

        dd("JSON has been converted and data saved on -> " . $sheet_identifier . " sheet");
      
        
    }
}
