<?php

namespace Genie\Schematojson\Schema\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Genie\Schematojson\Schema\Http\Requests\StructureRequest;
use Genie\Schematojson\Schema\Models\Structure;
use Illuminate\Support\Facades\Validator;
use Genie\Schematojson\Schema\Models\Storageconnection;

use Mpdf\Mpdf;

class StructureController extends StructureBaseController
{
    public function variableTypes()
    {
        $dbalDataType = ['Doctrine\DBAL\Types\IntegerType' => 'int',
        'Doctrine\DBAL\Types\StringType' => 'varchar',
        'Doctrine\DBAL\Types\DecimalType'=> 'decimal',
        "Doctrine\DBAL\Types\DateTimeType"=>'datetime',
        "Doctrine\DBAL\Types\TextType"=>'text',
        "Doctrine\DBAL\Types\DateType"=>'date',
        "Doctrine\DBAL\Types\JsonType"=>'json',
        "Doctrine\DBAL\Types\BigIntType"=>'bigint',
        "Doctrine\DBAL\Types\BooleanType"=>'boolean',
        "Doctrine\DBAL\Types\TimeType"=>'time',
        "Doctrine\DBAL\Types\SmallIntType"=>'smallint',
        "Doctrine\DBAL\Types\FloatType"=>'float',
        "Doctrine\DBAL\Types\DecimalType"=>'decimal',
        "Doctrine\DBAL\Types\BlobType"=>'varchar' //noted
        // "Doctrine\DBAL\Types\DateType"=>'date',
       ];      
       return $dbalDataType;
    }

    private function prepareConnection($storageconnection)
    {

        if(strtolower($storageconnection->storagetype_title)==='mysql'){
            $connection = [
                'driver' => strtolower($storageconnection->storagetype_title),
                'host' => $storageconnection->target,
                'port' => '3306',
                'database' => $storageconnection->storage,
                'username' => $storageconnection->client,
                'password' => $storageconnection->secret===null ? '':$storageconnection->secret,
            ];

        }
        if(strtolower($storageconnection->storagetype_title)==='oracle'){
            $connection = [
                'driver' => strtolower($storageconnection->storagetype_title),
                'host' => $storageconnection->target,
                'port' => '1521',
                'database' => $storageconnection->storage,
                'username' => $storageconnection->client,
                'password' => $storageconnection->secret===null ? '':$storageconnection->secret,
            ];
        }

        // $connection = [
        //     'driver' => strtolower($storageconnection->storagetype_title),
        //     'host' => $storageconnection->target,
        //     'port' => '1521',
        //     'database' => $storageconnection->storage,
        //     'username' => $storageconnection->client,
        //     'password' => $storageconnection->secret,
        // ];
        \Config::set('database.connections.'. $storageconnection->title, $connection);
        // dd(\Config::get('database.connections'));
            
        
    }


    public function structure($key, $uuid)
    {

        $storageconnection = Storageconnection::where('uuid', '=', $uuid)->first();
        // dd($storageconnection);
        $this->prepareConnection($storageconnection);
        
        $value = $key;
        $dbalDataType = $this->variableTypes();        
    
        $schema = DB::connection($storageconnection->title)->getDoctrineSchemaManager();
        // dd($storageconnection->title);
        $tables = $schema->listTableNames();
        
        $table = $tables[$key];
        $columns = $schema->listTableColumns($tables[$key]);
        // dd($columns);
        $types = [];
         foreach($columns as $key=>$column){
            // dd($column->toArray());
            $columnNames[$key] = $column->getType();
            $types[] = $dbalDataType[get_class($columnNames[$key])];
        }

        // dd($value);


    //   dd($dbalDataType[get_class($columnNames[4])]);
        return view('schema::structures.index',compact('columns','types','value','table'));
    }

    public function pdfStructure($key, $uuid)
    {
        $storageconnection = Storageconnection::where('uuid', '=', $uuid)->first();
        $this->prepareConnection($storageconnection);
        $value = $key;
        $dbalDataType = $this->variableTypes();        
    
        $schema = DB::connection($storageconnection->title)->getDoctrineSchemaManager();
        $tables = $schema->listTableNames();
        $table = $tables[$key];
        $columns = $schema->listTableColumns($tables[$key]);
        // dd($columns);
        $types = [];
         foreach($columns as $key=>$column){
            // dd($column->toArray());
            $columnNames[$key] = $column->getType();
            $types[] = $dbalDataType[get_class($columnNames[$key])];
        }

        $html = view('schema::structures.pdf',compact('columns','types','value','table'))->render();
        $mpdf = new Mpdf([
            'default_font_size' => 10,
        ]);
        
        
        $mpdf->WriteHTML($html);
        $mpdf->Output("{$table}.pdf", 'I');
    }

    public function editableStructure($key, $uuid)
    {
        $storageconnection = Storageconnection::where('uuid', '=', $uuid)->first();
        $this->prepareConnection($storageconnection);
        $dbalDataType = $this->variableTypes();   
    
        $schema = DB::connection($storageconnection->title)->getDoctrineSchemaManager();
        
        $tables = $schema->listTableNames();

        $table = $tables[$key];
        
        $columns = $schema->listTableColumns($tables[$key]);
        
        $types = [];
    
         foreach($columns as $key=>$column){
                $columnNames[$key] = $column->getType();
                $types[] = $dbalDataType[get_class($columnNames[$key])];
        }
        
        return view('schema::structures.editablestructure',compact('columns','types', 'table'));
    }

    public function selectedStructure($key, $uuid)
    {
        $storageconnection = Storageconnection::where('uuid', '=', $uuid)->first();
        $this->prepareConnection($storageconnection);
        $value = $key;
        $dbalDataType = $this->variableTypes();   
    
        $schema = DB::connection($storageconnection->title)->getDoctrineSchemaManager();
        
        $tables = $schema->listTableNames();

        $table = $tables[$key];
        
        $columns = $schema->listTableColumns($tables[$key]);
        
        $types = [];
    
         foreach($columns as $key=>$column){
                $columnNames[$key] = $column->getType();
                $types[] = $dbalDataType[get_class($columnNames[$key])];
        }
        
        return view('schema::structures.selectedstructure',compact('columns','types', 'table', 'value', 'uuid'));
    }

    public function selectedStructureJson(StructureRequest $request)
    {

        $key = $request->input('key');
        $uuid = $request->input('uuid');
        $columns = $request->input('column', []);
        $aleas = $request->input('aleas', []); 
        // $selected = $request->input('selected', []);
        $selected = $request->input('selected_items', '');
        $selectedArray = explode(',', $selected);

        $selectedArray = array_filter($selectedArray);
        
        // dd($selectedArray);

        $storageconnection = Storageconnection::where('uuid', '=', $uuid)->first();
        $this->prepareConnection($storageconnection);

        $value = $key;
        $dbalDataType = $this->variableTypes();        
    
        $schema = DB::connection($storageconnection->title)->getDoctrineSchemaManager();
        $connectionName = $storageconnection->title;

        $tables = $schema->listTableNames();
        $tableName = $tables[$key];    
        
         
        $columns = $schema->listTableColumns($tables[$key]);
        

        $matchedColumns = [];

        foreach ($selectedArray as $selectedColumn) {
            if (array_key_exists($selectedColumn, $columns)) {
                $matchedColumns[$selectedColumn] = $columns[$selectedColumn];
            }
        }

        // dd($columns, $selected, $matchedColumns);
        // dd($matchedColumns);

        $jsonData = [];
         foreach($matchedColumns as $column){
            $attrtibutes = $column->toArray();
            $columnNames[$key] = $column->getType();           
            if(strtolower($attrtibutes['name'])=='id'){
                $jsonData[strtolower($attrtibutes['name'])] = [                
                    'dataType' => $dbalDataType[get_class($columnNames[$key])],
                    'length' => $attrtibutes['length'] === 'NULL' ? null : ($attrtibutes['length'] === 191 ? 255 : $attrtibutes['length']),
                    'isPrimaryKey' => true,
                'miscellaneous' => [
                    'unsigned' => $attrtibutes['unsigned'],
                    'nullable' => $attrtibutes['notnull'] === true ? false : true,
                    'default' => $attrtibutes['default'] === 'NULL' ? null : $attrtibutes['default'],
                    'comment' => $attrtibutes['comment'] === 'NULL' ? null : $attrtibutes['comment']
                ],
                ];

            }else{
                $jsonData[strtolower($attrtibutes['name'])] = [                
                    'dataType' => $attrtibutes['length'] === 1 ? 'char' : $dbalDataType[get_class($columnNames[$key])],
                    'length' => $attrtibutes['length'] === 'NULL' ? null : ($attrtibutes['length'] === 191 ? 255 : $attrtibutes['length']),
                    'isPrimaryKey' => false,
                'miscellaneous' => [
                    'unsigned' => $attrtibutes['unsigned'],
                    'nullable' => $attrtibutes['notnull'] === true ? false : true,
                    'default' => $attrtibutes['default'] === 'NULL' ? null : $attrtibutes['default'],
                    'comment' => $attrtibutes['comment'] === 'NULL' ? null : $attrtibutes['comment']
                ],
                ];

            }

            if(strtolower($attrtibutes['name'])=='barcode' || strtolower($attrtibutes['name'])=='qrcode'){
                $jsonData[strtolower($attrtibutes['name'])] = [                
                    'dataType' => 'varchar',
                    'length' => 255,
                    'isPrimaryKey' => false,
                'miscellaneous' => [
                    'unsigned' => $attrtibutes['unsigned'],
                    'nullable' => $attrtibutes['notnull'] === true ? false : true,
                    'default' => $attrtibutes['default'] === 'NULL' ? null : $attrtibutes['default'],
                    'comment' => $attrtibutes['comment'] === 'NULL' ? null : $attrtibutes['comment']
                ],
                ];
            }
           }

        $jsonStructure = [
            "connection" => $connectionName,
            "tableName" =>  $tableName,
            "options" => [
                "engine" =>  'InnoDB',
                "charset" => 'utf8_general_ci',
            ],
            "columns" => $jsonData,
        ];    
        $response = response()->json($jsonStructure);
        $jsonData = $response->getData();
        return view('schema::structures.include-columns', compact('response','value','jsonData'));
    }

    public function editableStructurestore(StructureRequest $request)
    {

        $columns = $request->input('column', []); // Get the 'column' input data as an array
        $aleas = $request->input('aleas', []); // Get the 'aleas' input data as an array

        $columnAleasePairs = array_combine($columns, $aleas);

        dd($columnAleasePairs); 
    }

    public function jsonStructure($key, $uuid)
    {
        $storageconnection = Storageconnection::where('uuid', '=', $uuid)->first();
        $this->prepareConnection($storageconnection);
        $value = $key;
        $dbalDataType = $this->variableTypes();        
    
        $schema = DB::connection($storageconnection->title)->getDoctrineSchemaManager();
        $connectionName = $storageconnection->title;

        $tables = $schema->listTableNames();
        
        $tableName = strtolower($tables[$key]);    
       
        
         
        $columns = $schema->listTableColumns($tables[$key]);
        $jsonData = [];
         foreach($columns as $column){
            $attrtibutes = $column->toArray();
            $columnNames[$key] = $column->getType();  
            if(strtolower($attrtibutes['name'])=='id'){
                $jsonData[strtolower($attrtibutes['name'])] = [                
                    'dataType' => $dbalDataType[get_class($columnNames[$key])],
                    'length' => $attrtibutes['length'] === 'NULL' ? null : ($attrtibutes['length'] === 191 ? 255 : $attrtibutes['length']),
                    'isPrimaryKey' => true,
                'miscellaneous' => [
                    'unsigned' => $attrtibutes['unsigned'],
                    'nullable' => $attrtibutes['notnull'] === true ? false : true,
                    'default' => $attrtibutes['default'] === 'NULL' ? null : $attrtibutes['default'],
                    'comment' => $attrtibutes['comment'] === 'NULL' ? null : $attrtibutes['comment']
                ],
                ];

            }else{
                $jsonData[strtolower($attrtibutes['name'])] = [                
                    'dataType' => $attrtibutes['length'] === 1 ? 'char' : $dbalDataType[get_class($columnNames[$key])],
                    'length' => $attrtibutes['length'] === 'NULL' ? null : ($attrtibutes['length'] === 191 ? 255 : $attrtibutes['length']),
                    'isPrimaryKey' => false,
                'miscellaneous' => [
                    'unsigned' => $attrtibutes['unsigned'],
                    'nullable' => $attrtibutes['notnull'] === true ? false : true,
                    'default' => $attrtibutes['default'] === 'NULL' ? null : $attrtibutes['default'],
                    'comment' => $attrtibutes['comment'] === 'NULL' ? null : $attrtibutes['comment']
                ],
                ];

            }

            if(strtolower($attrtibutes['name'])=='barcode' || strtolower($attrtibutes['name'])=='qrcode'){
                $jsonData[strtolower($attrtibutes['name'])] = [                
                    'dataType' => 'varchar',
                    'length' => 255,
                    'isPrimaryKey' => false,
                'miscellaneous' => [
                    'unsigned' => $attrtibutes['unsigned'],
                    'nullable' => $attrtibutes['notnull'] === true ? false : true,
                    'default' => $attrtibutes['default'] === 'NULL' ? null : $attrtibutes['default'],
                    'comment' => $attrtibutes['comment'] === 'NULL' ? null : $attrtibutes['comment']
                ],
                ];
            }
           
           }

        $jsonStructure = [
            "connection" => $connectionName,
            "tableName" =>  $tableName,
            "options" => [
                "engine" =>  'InnoDB',
                "charset" => 'utf8_general_ci',
            ],
            "columns" => $jsonData,
        ];    
        $response = response()->json($jsonStructure);
        $jsonData = $response->getData();
        return view('schema::structures.include-columns', compact('response','value','jsonData'));
        
    }

    public function excelStore(StructureRequest $request)
    {
      
        // Structure::
        dd($request->all());
        return view('schema::structures.index',compact('jsonResponse','jsonData'));
    }

    public function includeColumn(StructureRequest $request){
        $executed_process = $request->executed_process;
        $data_12s = $request->data_12s;
        $data_signature_panel = $request->data_signature_panel;
        $json = $request->json;
        $key = $request->key;
        
        // Decode the existing JSON
        $existingJson = json_decode($json, true);
        // Decode the selected JSON
        $executedProcess = json_decode($executed_process, true);
        $data12s = json_decode($data_12s, true);
        $dataSignaturePanel = json_decode($data_signature_panel, true);
        // dd($executedProcess);

        if($executedProcess!=null){
            $existingJson['columns'] = array_merge($existingJson['columns'], $executedProcess);
        }
        if($data12s!=null){
            $existingJson['columns']['data_12ns'] = $data12s;
        }
        if($dataSignaturePanel!=null){
            $existingJson['columns']['data_signature_panel'] = $dataSignaturePanel;    
        }

        // Encode the updated JSON back to a string
        $updatedJson = json_encode($existingJson);

        // dd($updatedJson);

        return view('schema::structures.json', compact('updatedJson','key'));

       
    }
}
