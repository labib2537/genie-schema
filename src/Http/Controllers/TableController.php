<?php

namespace Genie\Schematojson\Schema\Http\Controllers;
use App\Http\Controllers\Controller;
use Genie\Schematojson\Schema\Http\Requests\TableRequest;
use Genie\Schematojson\Schema\Models\Table;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class TableController extends TableBaseController
{
    public function index()
    {
        
        $schema = DB::connection()->getDoctrineSchemaManager();
        $tables = $schema->listTableNames();
        // dd($tables);
        $columns = [];
        foreach($tables as $key => $table){
            $columns[] =$schema->listTableColumns($tables[$key]);
        }
        // dd($tables[0]);
        $columns = $schema->listTableColumns($tables[0]);
        // dd($columns);
      
        return view('schema::tables.index', compact('tables'));
    }

    public function tables($key)
    {

        $value = $key;
        $schema = DB::connection()->getDoctrineSchemaManager();
        $tables = $schema->listTableNames([$key]);
        dd($tables);
        // dd($key);


        return view('schema::tables.index', compact('tables','value'));
    }


    public function store(TableRequest $request)
    {
        try {
            $table = Table::create(['uuid'=>(string) Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('tables.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }

}
