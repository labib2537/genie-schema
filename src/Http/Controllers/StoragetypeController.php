<?php

namespace Genie\Schematojson\Schema\Http\Controllers;

use Genie\Schematojson\Schema\Http\Requests\StoragetypeRequest;
use Genie\Schematojson\Schema\Models\Storagetype;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;

class StoragetypeController extends StoragetypeBaseController
{
    public function store(StoragetypeRequest $request)
    {
        try {
            $storagetype = Storagetype::create(['uuid'=> (string)Str::uuid()] + $request->all());
            //handle relationship store
            return redirect()->route('storagetypes.index')
                ->withSuccess(__('Successfully Created'));
        } catch (\Exception | QueryException $e) {
            \Log::channel('pondit')->error($e->getMessage());
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }
}
