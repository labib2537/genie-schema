<?php 
use Illuminate\Support\Facades\Route;
use Genie\Schematojson\Schema\Http\Controllers\SchemaSheetController;
use Genie\Schematojson\Schema\Http\Controllers\StoragetypeController;
use Genie\Schematojson\Schema\Http\Controllers\TableController;
use Genie\Schematojson\Schema\Http\Controllers\StructureController;


use Genie\Schematojson\Schema\Http\Controllers\StorageconnectionController;
//use namespace

Route::group(['middleware' => 'web'], function () {
	Route::resource('storagetypes', StoragetypeController::class);
	Route::resource('storageconnections', StorageconnectionController::class);
	Route::resource('tables', TableController::class);
	Route::resource('json_structures', StructureController::class);

//Place your route here
Route::post('createJson', [SchemaSheetController::class, 'create'])->name('create_json');
Route::post('store', [SchemaSheetController::class, 'store'])->name('json_store');
Route::post('select', [SchemaSheetController::class, 'selectTable'])->name('json_select');
// Route::get('connect/{datasource}', [StorageconnectionController::class, 'connect'])->name('inputSpreadSheet');
Route::get('connect/{uuid}', [StorageconnectionController::class, 'connect'])->name('storageconnection');

Route::get('/columns/{key}/{uuid}',[StructureController::class, 'structure'])->name('structure');
Route::get('/editablestructure/{key}/{uuid}',[StructureController::class, 'editablestructure'])->name('editablestructure');
Route::post('/editablestructure',[StructureController::class, 'editablestructurestore'])->name('editablestructure.store');
Route::get('/json-structure/{key}/{uuid}',[StructureController::class, 'jsonStructure'])->name('structures.json');
Route::get('/table/{key}',[TableController::class, 'tables'])->name('table');
Route::get('/selectedstructure/{key}/{uuid}',[StructureController::class, 'selectedStructure'])->name('selectedstructure');
Route::post('/selectedStructureJson',[StructureController::class, 'selectedStructureJson'])->name('selectedStructure.json');
Route::get('/pdf-structure/{key}/{uuid}',[StructureController::class, 'pdfStructure'])->name('structure.pdf');
Route::post('oracle/input-spreadsheet',[SchemaSheetController::class, 'inputSpreadsheetForStore'])->name('input_json_to_excel');
Route::post('oracle/select-table',[SchemaSheetController::class, 'selectSheetForStore'])->name('selectSheet_json_to_excel');
Route::post('oracle/store-excel',[SchemaSheetController::class, 'storeExcel'])->name('store_json_to_excel');
Route::post('oracle/include-columns',[StructureController::class, 'includeColumn'])->name('include_column_json');




});