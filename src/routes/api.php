<?php 
use Illuminate\Support\Facades\Route;
use Genie\Schematojson\Schema\Http\Controllers\Api\StoragetypeController;
use Genie\Schematojson\Schema\Http\Controllers\Api\StoragetypeHistoryController;
use Genie\Schematojson\Schema\Http\Controllers\Api\StorageconnectionController;

use Genie\Schematojson\Schema\Http\Controllers\Api\StorageconnectionHistoryController;
//use namespace

Route::group(['middleware' => 'api', 'prefix' => 'api', 'as' => 'api.'], function () {
	Route::apiResource('storagetypes', StoragetypeController::class);
	Route::get('storagetype-histories-list/{uuid}', [StoragetypeHistoryController::class,'list']);
	Route::apiResource('storagetype-histories', StoragetypeHistoryController::class);
	Route::apiResource('storageconnections', StorageconnectionController::class);
	Route::get('storageconnection-histories-list/{uuid}', [StorageconnectionHistoryController::class,'list']);
	Route::apiResource('storageconnection-histories', StorageconnectionHistoryController::class);
//Place your route here
});