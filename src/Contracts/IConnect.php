<?php

namespace Genie\Schematojson\Schema\Contracts;

interface IConnect
{
    public function connect($connection, $uuid);
    
}
