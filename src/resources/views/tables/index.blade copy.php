<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Tables') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-message :message="session('success')" type="success" />
            <x-sg-table type="basic"  id="tableDatatable">
                <x-sg-thead>
                    <tr>
                        <th>{{ __('SL') }}</th>
                            						<th>{{ __('Title') }}</th>
						<th>{{ __('Rows') }}</th>
						<th>{{ __('Type') }}</th>
						<th>{{ __('Data length') }}</th>
						<th>{{ __('Auto increment') }}</th>
						<th>{{ __('Comment') }}</th>

                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-sg-thead>
                <x-sg-tbody>
                    @foreach ($tables as $table)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        						<td>{{ $table->title }}</td>
						<td>{{ $table->rows }}</td>
						<td>{{ $table->type }}</td>
						<td>{{ $table->datalength }}</td>
						<td>{{ $table->autoincrement }}</td>
						<td>{{ $table->comment }}</td>

                        <td>
                            <x-sg-link-show href="{{route('tables.show', $table->uuid)}}" />
                            <x-sg-link-edit href="{{route('tables.edit', $table->uuid)}}" />
                            <x-sg-btn-delete action="{{route('tables.destroy', $table->uuid)}}" method="post" />
                            <x-sg-link-history uuid="{{ $table->uuid }}"  api="/api/table-histories-list" />
                        </td>
                    </tr>
                    @endforeach
                </x-sg-tbody>
            </x-sg-table>
        </x-slot>
        <x-slot name="cardFooterCenter">

            <x-sg-link-create href="{{route('tables.create')}}" />

        </x-slot>
    </x-sg-card>



@push('js')


<script>
   $(document).ready(function() {
        $('#tableDatatable').DataTable({
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="icon-grid3"></i>',
                    className: 'btn bg-indigo-400 btn-icon dropdown-toggle'
                }
            ],
            stateSave: false,
            columnDefs: [
                {
                    targets: 0,
                    visible: true
                }
            ]
        });
    });
</script>
@endpush

</x-sg-master>
