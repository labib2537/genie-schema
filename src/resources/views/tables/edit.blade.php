<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Table') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-errors :errors="$errors" />
            <form action="{{ route('tables.update', $table->uuid) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="row">
                    {{--relationalFields--}}
                    <!-- title -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="titleInput">{{ __('Title') }}</x-sg-label>
        <x-sg-text type="text" id="titleInput" name="title" :value="old('title', $table->title)" placeholder="" />
        <x-sg-alert-errors name="title" />
    </div>
</div>

<!-- rows -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="rowsInput">{{ __('Rows') }}</x-sg-label>
        <x-sg-text type="text" id="rowsInput" name="rows" :value="old('rows', $table->rows)" placeholder="" />
        <x-sg-alert-errors name="rows" />
    </div>
</div>

<!-- type -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="typeInput">{{ __('Type') }}</x-sg-label>
        <x-sg-text type="text" id="typeInput" name="type" :value="old('type', $table->type)" placeholder="" />
        <x-sg-alert-errors name="type" />
    </div>
</div>

<!-- collation -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="collationInput">{{ __('Collation') }}</x-sg-label>
        <x-sg-text type="text" id="collationInput" name="collation" :value="old('collation', $table->collation)" placeholder="" />
        <x-sg-alert-errors name="collation" />
    </div>
</div>

<!-- data length -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="data lengthInput">{{ __('Data length') }}</x-sg-label>
        <x-sg-text type="text" id="data lengthInput" name="data length" :value="old('data length', $table->data length)" placeholder="" />
        <x-sg-alert-errors name="data length" />
    </div>
</div>

<!-- index length -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="index lengthInput">{{ __('Index length') }}</x-sg-label>
        <x-sg-text type="text" id="index lengthInput" name="index length" :value="old('index length', $table->index length)" placeholder="" />
        <x-sg-alert-errors name="index length" />
    </div>
</div>

<!-- data free -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="data freeInput">{{ __('Data free') }}</x-sg-label>
        <x-sg-text type="text" id="data freeInput" name="data free" :value="old('data free', $table->data free)" placeholder="" />
        <x-sg-alert-errors name="data free" />
    </div>
</div>

<!-- auto increment -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="auto incrementInput">{{ __('Auto increment') }}</x-sg-label>
        <x-sg-text type="text" id="auto incrementInput" name="auto increment" :value="old('auto increment', $table->auto increment)" placeholder="" />
        <x-sg-alert-errors name="auto increment" />
    </div>
</div>

<!-- comment -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="commentInput">{{ __('Comment') }}</x-sg-label>
        <x-sg-text type="text" id="commentInput" name="comment" :value="old('comment', $table->comment)" placeholder="" />
        <x-sg-alert-errors name="comment" />
    </div>
</div>



                    <div class = "{{$decoration['class']['formfooter']}}" >
                        <x-sg-btn-submit />
                        <x-sg-btn-cancel />
                    </div>
                </div>
            </form>
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-list href="{{route('tables.index')}}" />
        </x-slot>
    </x-sg-card>
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush

</x-sg-master>
