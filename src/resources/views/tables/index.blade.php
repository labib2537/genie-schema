<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Tables') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-message :message="session('success')" type="success" />
            <x-sg-table type="basic"  id="tableDatatable">
                <x-sg-thead>
                    <tr>
                        <th>{{ __('SL') }}</th>
                        <th>{{ __('Title') }}</th>
                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-sg-thead>
                <x-sg-tbody>
                    @foreach ($tables as $key=>$table)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $table }}</td>
						
                        <td>
                            <x-sg-link-show href="{{route('structure', [$key, $uuid])}}" />
                            <a class="btn btn-sm bg-grey-800 border-2 border-grey-800 btn-icon rounded-round legitRipple shadow mr-1" href="{{route('structures.json', [$key, $uuid] )}}">
                                <i class="icon-file-css2"></i></a>
                            <x-sg-link-edit href="{{route('editablestructure',  [$key, $uuid])}}" />
                            <a class="btn btn-sm bg-pink-800 border-2 border-pink-800 btn-icon rounded-round legitRipple shadow mr-1" href="{{route('selectedstructure',  [$key, $uuid])}}">
                                <i class="icon-file-check2"></i></a>
                            <x-sg-link-pdf href="{{route('structure.pdf',  [$key, $uuid])}}" />

                          
                        </td>
                    </tr>
                    @endforeach
                </x-sg-tbody>
            </x-sg-table>
        </x-slot>
        <x-slot name="cardFooterCenter">

            <x-sg-link-create href="{{route('tables.create')}}" />

        </x-slot>
    </x-sg-card>



@push('js')


<script>
   $(document).ready(function() {
        $('#tableDatatable').DataTable({
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="icon-grid3"></i>',
                    className: 'btn bg-indigo-400 btn-icon dropdown-toggle'
                }
            ],
            stateSave: false,
            columnDefs: [
                {
                    targets: 0,
                    visible: true
                }
            ]
        });
    });
</script>
@endpush

</x-sg-master>
