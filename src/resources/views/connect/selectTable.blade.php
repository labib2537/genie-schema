<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Selete Table') }}
        </x-slot>
        <x-slot name="body">
  
  @if($errorMessage)
    <p class="my-3">Spreadsheet ID has not Found!!</p>
  @else  

  <form action="{{ route('create_json') }}" method="post">
    @csrf
    <div class="mb-3">
    <label for="spreadSheet" class="form-label">Input Spreadsheet ID</label>
    <input type="text" class="form-control" name="spreadSheet" value="{{$spreadsheet_id}}" readonly>
    </div>
    <select name="sheet" class="form-control form-control-lg" required>
    <option value="" selected>Select Sheet</option>
    @foreach($tableData as $key=>$table)
    <option value="{{$table}}">{{$table}}</option>
    @endforeach
    </select>
    <button type="submit" class="btn btn-success my-3"><i class="fas fa-eye mr-2"></i>Show</button>
    </form>
    @endif
    </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}

@endpush

</x-sg-master>