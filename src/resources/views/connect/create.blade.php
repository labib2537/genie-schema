<?php
$jsonString = json_encode($jsonData);
?>
<x-sg-master>
     <!-- Include Prism.js and its dark theme -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/themes/prism-okaidia.min.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/prism.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/components/prism-json.min.js"></script>
 
     <style>
         /* Override Prism.js CSS for JSON to remove white background behind colons */
         .language-json .token.operator {
             background: none !important;
         }
     </style>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('JSON DATA STORE') }}
        </x-slot>
        <x-slot name="body">

            <form action="{{ route('json_store') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Pakeage ID</label>
                    <input type="text" class="form-control" name="pkg_id"
                        value="{{ $jsonResponse->headers->get('package-id') }}" readonly>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Entity</label>
                    <input type="text" class="form-control" name="entity" value="{{ $jsonData->tableName }}"
                        readonly>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Connection Name</label>
                    <input type="text" class="form-control" name="conn_name" value="{{ $jsonData->connection }}"
                        readonly>
                </div>
                <div class="mb-3">
                    {{-- <label for="exampleInputPassword1" class="form-label">Json Migration</label> --}}
                    <input type="hidden" class="form-control" name="json_migration" value="{{ $jsonString }}"
                        readonly>
                </div>
                <button type="submit" class="btn btn-success my-3"><i class="fas fa-save mr-2"></i>STORE</button>
            </form>
            <div class="position-relative">
                <pre id="jsonContent" class="language-json"></pre>
                <button id="copyButton" class="btn btn-secondary position-absolute top-0 right-0 m-1">
                    <i class="fas fa-copy"></i>
                </button>
                <button id="changeBgButton" class="btn btn-secondary position-absolute top-0 right-0 mt-1 mr-5">
                    <i class="icon-contrast"></i>
                </button>
                <div id="copyTooltip"
                    class="position-absolute top-0 right-0 m-2 bg-success text-white rounded px-2 py-1"
                    style="display: none;">Copied!</div>
            </div>

            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
            </script>
            <script>
                var jsonString = <?php echo json_encode($jsonString); ?>;
                document.getElementById("jsonContent").textContent = JSON.stringify(JSON.parse(jsonString), null, 2);
                Prism.highlightElement(document.getElementById("jsonContent"));

                document.getElementById("copyButton").addEventListener("click", function() {
                    // Select the text in the pre element
                    var textToCopy = document.getElementById("jsonContent");
                    var range = document.createRange();
                    range.selectNodeContents(textToCopy);
                    var selection = window.getSelection();
                    selection.removeAllRanges();
                    selection.addRange(range);

                    document.execCommand("copy");

                    selection.removeAllRanges();

                    // alert("Copied to clipboard: " + textToCopy.textContent);
                    // Show the tooltip
                    var tooltip = document.getElementById("copyTooltip");
                    tooltip.style.display = "block";

                    // Hide the tooltip after a short delay
                    setTimeout(function() {
                        tooltip.style.display = "none";
                    }, 2000); // 2000 milliseconds (2 seconds)

                });
                 // Initialize the background mode
        var darkMode = true;

// Change BG button click event listener
document.getElementById("changeBgButton").addEventListener("click", function () {
    var jsonContent = document.getElementById("jsonContent");
    darkMode = !darkMode; // Toggle darkMode
    if(darkMode){
        jsonContent.style.backgroundColor = "#1e1e1e";
        jsonContent.style.color = "#d4d4d4"; 
    }else{
        jsonContent.style.backgroundColor = "#003333"; 
        jsonContent.style.color = "#000000";
    }

});
            </script>
        </x-slot>
    </x-sg-card>

    @push('css')
        {{-- pagespecific-css --}}
    @endpush

    @push('js')
        {{-- pagespecific-js --}}
    @endpush

</x-sg-master>
