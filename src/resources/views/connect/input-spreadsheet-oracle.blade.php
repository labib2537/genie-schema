<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Input Spreadsheet to Store') }}
        </x-slot>
        <x-slot name="body">
  
  <div class="heading" style="text-align: center;">
    <h4 class="my-3"></h4>
  </div>
    <form action="{{ route('selectSheet_json_to_excel') }}" method="post">
      @csrf
    <input type="hidden" class="form-control" name="json" value="{{$json}}">

    <div class="mb-3">
    <label for="spreadSheet" class="form-label">Input Spreadsheet ID</label>
    <input type="text" class="form-control" name="spreadSheet">
    </div>
    <button type="submit" class="btn btn-primary"><i class="fas fa-check mr-2"></i>Submit</button>
    </form>
  </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush

</x-sg-master>