<x-sg-master>
   <!-- Include Prism.js and its dark theme -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/themes/prism-okaidia.min.css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/prism.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/components/prism-json.min.js"></script>

   <style>
       /* Override Prism.js CSS for JSON to remove white background behind colons */
       .language-json .token.operator {
           background: none !important;
       }
   </style>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Selete Sheet to Store') }}
        </x-slot>
        <x-slot name="body">
  
  @if($errorMessage)
    <p class="my-3">Spreadsheet ID has not Found!!</p>
  @else  

  <form action="{{ route('store_json_to_excel') }}" method="post">
    @csrf
    <input type="hidden" class="form-control" name="json" value="{{$json}}">

    <div class="mb-3">
    <label for="spreadSheet" class="form-label">Input Spreadsheet ID</label>
    <input type="text" class="form-control" name="spreadSheet" value="{{$spreadsheet_id}}" readonly>
    </div>
    <select name="sheet" class="form-control form-control-lg" required>
    <option value="" selected>Select Sheet</option>
    @foreach($tableData as $key=>$table)
    <option value="{{$table}}">{{$table}}</option>
    @endforeach
    </select>
    <button type="submit" class="btn btn-success my-3"><i class="fas fa-save mr-2"></i>Store</button>
    </form>
    <div class="position-relative">
      <pre id="jsonContent" class="language-json"></pre>
      <button id="copyButton" class="btn btn-secondary position-absolute top-0 right-0 m-1">
          <i class="fas fa-copy"></i>
      </button>
      <div id="copyTooltip"
          class="position-absolute top-0 right-0 m-2 bg-success text-white rounded px-2 py-1"
          style="display: none;">Copied!</div>
  </div>

    @endif

    </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
<script>
var json = <?php echo json_encode($json); ?>;
document.getElementById("jsonContent").textContent = JSON.stringify(JSON.parse(json), null, 2);

Prism.highlightElement(document.getElementById("jsonContent"));

        document.getElementById("copyButton").addEventListener("click", function () {
            var textToCopy = document.getElementById("jsonContent");
            var range = document.createRange();
            range.selectNodeContents(textToCopy);
            var selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);

            document.execCommand("copy");

            selection.removeAllRanges();

            var tooltip = document.getElementById("copyTooltip");
            tooltip.style.display = "block";

            setTimeout(function () {
                tooltip.style.display = "none";
            }, 2000);
        });

</script>

</x-sg-master>