<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('StorageType') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-errors :errors="$errors" />
            <form action="{{ route('storagetypes.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                {{--relationalFields--}}

                    <!-- title -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="titleInput">{{ __('Title') }}</x-sg-label>
        <x-sg-text type="text" id="titleInput" name="title" :value="old('title')" placeholder="" />
        <x-sg-alert-errors name="title" />
    </div>
</div>

<!-- type -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="typeInput">{{ __('Type') }}</x-sg-label>
        <!-- <x-sg-text type="text" id="typeInput" name="type" :value="old('type')" placeholder="" /> -->
        @php
            $types = ["SQL", "NoSQL", "SASS"];
        @endphp

        <select name="type" class="form-control form-control-lg">
			@foreach($types as $type)
            <option>{{ $type }}</option>
            @endforeach
		</select>
        <x-sg-alert-errors name="type" />
    </div>
</div>

<!-- description -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="descriptionInput">{{ __('Description') }}</x-sg-label>
        <x-sg-text type="text" id="descriptionInput" name="description" :value="old('description')" placeholder="" />
        <x-sg-alert-errors name="description" />
    </div>
</div>



                    <div class = "{{$decoration['class']['formfooter']}}" >
                        <x-sg-btn-submit />
                        <x-sg-btn-cancel />
                    </div>
                </div>
            </form>
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-list href="{{route('storagetypes.index')}}" />
        </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush

</x-sg-master>
