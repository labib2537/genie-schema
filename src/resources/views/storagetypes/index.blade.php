<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('StorageType') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-message :message="session('success')" type="success" />
            <x-sg-table type="basic"  id="storagetypeDatatable">
                <x-sg-thead>
                    <tr>
                        <th>{{ __('SL') }}</th>
                            						<th>{{ __('Title') }}</th>
						<th>{{ __('Type') }}</th>

                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-sg-thead>
                <x-sg-tbody>
                    @foreach ($storagetypes as $storagetype)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        						<td>{{ $storagetype->title }}</td>
						<td>{{ $storagetype->type }}</td>

                        <td>
                            <x-sg-link-show href="{{route('storagetypes.show', $storagetype->uuid)}}" />
                            <x-sg-link-edit href="{{route('storagetypes.edit', $storagetype->uuid)}}" />
                            <x-sg-btn-delete action="{{route('storagetypes.destroy', $storagetype->uuid)}}" method="post" />
                            <x-sg-link-history uuid="{{ $storagetype->uuid }}"  api="/api/storagetype-histories-list" />
                        </td>
                    </tr>
                    @endforeach
                </x-sg-tbody>
            </x-sg-table>
        </x-slot>
        <x-slot name="cardFooterCenter">

            <x-sg-link-create href="{{route('storagetypes.create')}}" />

        </x-slot>
    </x-sg-card>



@push('js')


<script>
   $(document).ready(function() {
        $('#storagetypeDatatable').DataTable({
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="icon-grid3"></i>',
                    className: 'btn bg-indigo-400 btn-icon dropdown-toggle'
                }
            ],
            stateSave: false,
            columnDefs: [
                {
                    targets: 0,
                    visible: true
                }
            ]
        });
    });
</script>
@endpush

</x-sg-master>
