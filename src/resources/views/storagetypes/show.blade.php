<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('StorageType') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Title') }} : </b> {{ $storagetype->title }}</p>
			<p><b>{{ __('Type') }} : </b> {{ $storagetype->type }}</p>
			<p><b>{{ __('Description') }} : </b> {{ $storagetype->description }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-create href="{{route('storagetypes.index')}}" />
            <x-sg-link-list href="{{route('storagetypes.index')}}" />
            <x-sg-link-edit href="{{route('storagetypes.edit',$storagetype->uuid)}}" />
            <x-sg-link-history uuid="{{$storagetype->uuid}}" api="/api/storagetypes-histories-list" />
        </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-sg-master>
