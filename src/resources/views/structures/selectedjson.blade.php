<x-sg-master>
    <!-- Include Prism.js and its dark theme -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/themes/prism-okaidia.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/prism.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/components/prism-json.min.js"></script>

    <style>
        /* Override Prism.js CSS for JSON to remove white background behind colons */
        .language-json .token.operator {
            background: none !important;
        }
    </style>

    <x-sg-card>
        <x-slot name="heading">
            {{ __('JSON Structure') }}
        </x-slot>
        <x-slot name="body">
            <form action="{{ route('input_json_to_excel') }}" method="post">
                @csrf
                <input type="hidden" name="json" value="{{$updatedJson}}">
                <input type="hidden" name="key" value="{{$key}}">
                <button type="submit" class="btn btn-success my-3">
                    <i class="fas fa-file-excel mr-2"></i>Store to Excel
                </button>
            </form>

            <div class="position-relative">
                <pre id="jsonContent" class="language-json"></pre>
                <button id="copyButton" class="btn btn-secondary position-absolute top-0 right-0 m-1">
                    <i class="fas fa-copy"></i>
                </button>
                <button id="changeBgButton" class="btn btn-secondary position-absolute top-0 right-0 mt-1 mr-5">
                    <i class="icon-contrast"></i>
                </button>
                <div id="copyTooltip"
                    class="position-absolute top-0 right-0 m-2 bg-success text-white rounded px-2 py-1"
                    style="display: none;">Copied!</div>
            </div>

        
            
        </x-slot>
    </x-sg-card>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
    <script>
        var jsonString = <?php echo json_encode($updatedJson); ?>;
        document.getElementById("jsonContent").textContent = JSON.stringify(JSON.parse(jsonString), null, 2);
        Prism.highlightElement(document.getElementById("jsonContent"));

        document.getElementById("copyButton").addEventListener("click", function () {
            var textToCopy = document.getElementById("jsonContent");
            var range = document.createRange();
            range.selectNodeContents(textToCopy);
            var selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);

            document.execCommand("copy");

            selection.removeAllRanges();

            var tooltip = document.getElementById("copyTooltip");
            tooltip.style.display = "block";

            setTimeout(function () {
                tooltip.style.display = "none";
            }, 2000);
        });

        // Initialize the background mode
        var darkMode = true;

        // Change BG button click event listener
        document.getElementById("changeBgButton").addEventListener("click", function () {
            var jsonContent = document.getElementById("jsonContent");
            darkMode = !darkMode; // Toggle darkMode
            if(darkMode){
                jsonContent.style.backgroundColor = "#1e1e1e";
                jsonContent.style.color = "#d4d4d4"; 
            }else{
                jsonContent.style.backgroundColor = "#003333"; 
                jsonContent.style.color = "#000000";
            }

        });
    </script>
</x-sg-master>
