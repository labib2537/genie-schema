<x-sg-master>
    @php
    $jsonString = json_encode($jsonData);
    @endphp
     <!-- Include Prism.js and its dark theme -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/themes/prism-okaidia.min.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/prism.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.24.1/components/prism-json.min.js"></script>
 
     <style>
         /* Override Prism.js CSS for JSON to remove white background behind colons */
         .language-json .token.operator {
             background: none !important;
         }
     </style>
<x-sg-card>
    <x-slot name="heading">
        {{ __('JSON Structure') }}
    </x-slot>
    <x-slot name="body">
        <h4>Select checkbox to include columns</h4>
        <form action="{{ route('include_column_json') }}" method="post">
            @csrf
        <input type="checkbox" name="executed_process"
        value='{
            "executed_process_id": {
              "dataType": "bigint",
              "length": null,
              "isPrimaryKey": false,
              "miscellaneous": {
                "unsigned": true,
                "nullable": true,
                "default": null,
                "comment": null
              }
            },
            "executed_process_uuid": {
              "dataType": "varchar",
              "length": 36,
              "isPrimaryKey": false,
              "miscellaneous": {
                "unsigned": false,
                "nullable": true,
                "default": null,
                "comment": null
              }
            },
            "executed_process_title": {
              "dataType": "varchar",
              "length": 255,
              "isPrimaryKey": false,
              "miscellaneous": {
                "unsigned": false,
                "nullable": true,
                "default": null,
                "comment": null
              }
            }
          }'>
         <span class="text" style="font-size: 15px;" >Include execuded_process</span><br>
        <input type="checkbox" name="data_12s" value='{"dataType":"text","length":null,"isPrimaryKey":false,"miscellaneous":{"unsigned":false,"nullable":true,"default":null,"comment":null}}'>
        <span class="text" style="font-size: 15px;" > Include data_12ns</span><br>
        <input type="checkbox" name="data_signature_panel" value='{"dataType":"text","length":null,"isPrimaryKey":false,"miscellaneous":{"unsigned":false,"nullable":true,"default":null,"comment":null}}'>
        <span class="text" style="font-size: 15px;" > Include Data_signature_panel</span><br>

        
            <input type="hidden" name="json" value="{{$jsonString}}">
            <input type="hidden" name="key" value="{{$value}}">
            <button type="submit" class="btn btn-primary my-3">
                <i class="icon-check mr-1"></i>Submit
            </button>
        </form>

        <div class="position-relative">
          <pre id="jsonContent" class="language-json"></pre>
          <button id="copyButton" class="btn btn-secondary position-absolute top-0 right-0 m-1">
              <i class="fas fa-copy"></i>
          </button>
          <button id="changeBgButton" class="btn btn-secondary position-absolute top-0 right-0 mt-1 mr-5">
            <i class="icon-contrast"></i>
        </button>
          <div id="copyTooltip"
              class="position-absolute top-0 right-0 m-2 bg-success text-white rounded px-2 py-1"
              style="display: none;">Copied!</div>
      </div>
        
    </x-slot>
</x-sg-card>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
        <script>
        var jsonString = <?php echo json_encode($jsonString); ?>;
        document.getElementById("jsonContent").textContent = JSON.stringify(JSON.parse(jsonString), null, 2);
        Prism.highlightElement(document.getElementById("jsonContent"));

        document.getElementById("copyButton").addEventListener("click", function () {
            var textToCopy = document.getElementById("jsonContent");
            var range = document.createRange();
            range.selectNodeContents(textToCopy);
            var selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);

            document.execCommand("copy");

            selection.removeAllRanges();

            var tooltip = document.getElementById("copyTooltip");
            tooltip.style.display = "block";

            setTimeout(function () {
                tooltip.style.display = "none";
            }, 2000);
        });

        // Initialize the background mode
        var darkMode = true;

        // Change BG button click event listener
        document.getElementById("changeBgButton").addEventListener("click", function () {
            var jsonContent = document.getElementById("jsonContent");
            darkMode = !darkMode; // Toggle darkMode
            if(darkMode){
                jsonContent.style.backgroundColor = "#1e1e1e";
                jsonContent.style.color = "#d4d4d4"; 
            }else{
                jsonContent.style.backgroundColor = "#003333"; 
                jsonContent.style.color = "#000000";
            }

        });


        
      </script>
</x-sg-master>