<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$table}} Table</title>
    <style>
        table {
          border-collapse: collapse;
          width: 100%;
        }
      
        th, td {
          border: 1px solid black;
          padding: 8px;
          text-align: left;
        }

        h5{
            text-align: center;
        }
      </style>
      
  </head>
  <body>
    
    
        <h5>{{$table}}</h5>
        
        <table>
            <thead>
              <tr>
                <th><?= ucfirst('SL') ?></th>
                <th><?= ucfirst('Column') ?></th>
                <th><?= ucfirst('Data Type') ?></th>
                <th><?= ucfirst('Lenght') ?></th>
                <th><?= ucfirst('Default') ?></th>
                <th><?= ucfirst('unsigned') ?></th>
                <th><?= ucfirst('comment') ?></th>
                <th><?= ucfirst('not null') ?></th>				
                <th><?= ucfirst('precision') ?></th>
                <th><?= ucfirst('scale') ?></th>
                <th><?= ucfirst('autoincrement') ?></th>
                <th><?= ucfirst('columnDefinition') ?></th>
              </tr>
            </thead>
            <tbody>
                @php                  
                $counter = 0;                       
            @endphp
            @foreach ($columns as $index=>$column)
            @php
                $attr = $column->toArray();
            @endphp
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{$index}}</td>
                <td>{{$types[$counter]}}</td>
                <td>{{$attr['length']}}</td>						
                <td>{{$attr['default']?'Y':'N'}}</td>						
                <td>{{$attr['unsigned']?'Y':'N'}}</td>						
                <td>{{$attr['comment']}}</td>						
                <td>{{$attr['notnull']?'Y':'N'}}</td>						
                <td>{{$attr['precision']}}</td>						
                <td>{{$attr['scale']}}</td>						
                <td>{{$attr['autoincrement']?'Y':'N'}}</td>						
                <td>{{$attr['columnDefinition']}}</td>						
            </tr>
            
            @endforeach
              
            </tbody>
          </table>
    </div>
  </body>
</html>

