<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('Selectable Columm Structure (' . $table . ')') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-message :message="session('success')" type="success" />
            <form action="{{route('selectedStructure.json')}}" method="post">
                @csrf
                <input type="hidden" name="key" value="{{ $value }}">
                <input type="hidden" name="uuid" value="{{ $uuid }}">
                <input type="hidden" name="selected_items" id="selectedItems" value="">
                <x-sg-table type="basic"  id="structureDatatable">
                    <x-sg-thead>
                        <tr>
                            <th>{{ __('SL') }}</th>
                            <th>{{ __('Column') }}</th>
                            <th>{{ __('Alease') }}</th>
                            <th>{{ __('Selected') }}</th>
                            
                            
                        </tr>
                    </x-sg-thead>
                    <x-sg-tbody>
                        @php                  
                            $counter = 0;                       
                        @endphp
                        @foreach ($columns as $index=>$column)
                        @php
                            $attr = $column->toArray();
                        @endphp
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                {{$index}}
                                <input type="hidden" name="column[]" value="{{ $index }}">
                                
                            </td>
                            <td>
                               
                                <input type="text" name="aleas[]" value="{{  implode(' ', array_map('ucfirst', explode('_',$index))) }}" class="form-control">
                            </td>
                            <td>
                                <input type="checkbox" name="selected[]" id="selected" class="form-control" value="{{ $index }}">
                            </td>
                            										
                        </tr>
                        @php
                            $counter++
                        @endphp
                        @endforeach
                    </x-sg-tbody>
                </x-sg-table>

                <div class = "{{$decoration['class']['formfooter']}}" >
                    <x-sg-btn-submit />
                    <x-sg-btn-cancel />
                </div>
            </form>
        </x-slot>
        <x-slot name="cardFooterCenter">

            <x-sg-link-create href="{{route('structures.create')}}" />

        </x-slot>
    </x-sg-card>



@push('js')


<script>
    document.addEventListener("DOMContentLoaded", function () {
        const form = document.getElementById("myForm");
        const checkboxes = document.querySelectorAll('input[name="selected[]"]');
        const selectedItemsInput = document.getElementById("selectedItems");

        checkboxes.forEach((checkbox) => {
            checkbox.addEventListener("change", function () {
                const selectedItems = Array.from(checkboxes)
                    .filter((cb) => cb.checked)
                    .map((cb) => cb.value);

                // update the hidden input field with the selected items
                selectedItemsInput.value = selectedItems.join(",");
            });
        });
    });



    
   $(document).ready(function() {
        $('#structureDatatable').DataTable({
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="icon-grid3"></i>',
                    className: 'btn bg-indigo-400 btn-icon dropdown-toggle'
                }
            ],
            stateSave: false,
            columnDefs: [
                {
                    targets: 0,
                    visible: true
                }
            ]
        });
    });
</script>
@endpush

</x-sg-master>
