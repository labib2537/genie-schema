<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('StorageConnection') }}
        </x-slot>
        <x-slot name="body">
            <p><b>{{ __('Title') }} : </b> {{ $storageconnection->title }}</p>
			<p><b>{{ __('Storagetype Id') }} : </b> {{ $storageconnection->storagetype_id }}</p>
			<p><b>{{ __('Storagetype Uuid') }} : </b> {{ $storageconnection->storagetype_uuid }}</p>
			<p><b>{{ __('Storagetype Title') }} : </b> {{ $storageconnection->storagetype_title }}</p>
			<p><b>{{ __('Target') }} : </b> {{ $storageconnection->target }}</p>
			<p><b>{{ __('Storage') }} : </b> {{ $storageconnection->storage }}</p>
			<p><b>{{ __('Client') }} : </b> {{ $storageconnection->client }}</p>
			<p><b>{{ __('Secret') }} : </b> {{ $storageconnection->secret }}</p>
			<p><b>{{ __('Encription') }} : </b> {{ $storageconnection->encription }}</p>
			<p><b>{{ __('File Path') }} : </b> {{ $storageconnection->file_path }}</p>
			<p><b>{{ __('File Content') }} : </b> {{ $storageconnection->file_content }}</p>
			<p><b>{{ __('Scope') }} : </b> {{ $storageconnection->scope }}</p>
			<p><b>{{ __('Data Connection') }} : </b> {{ $storageconnection->data_connection }}</p>
			<p><b>{{ __('Qrcode') }} : </b> {{ $storageconnection->qrcode }}</p>
			
            {{--othersInfo--}}
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-create href="{{route('storageconnections.index')}}" />
            <x-sg-link-list href="{{route('storageconnections.index')}}" />
            <x-sg-link-edit href="{{route('storageconnections.edit',$storageconnection->uuid)}}" />
            <x-sg-link-history uuid="{{$storageconnection->uuid}}" api="/api/storageconnections-histories-list" />
        </x-slot>
    </x-sg-card>

@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush
</x-sg-master>
