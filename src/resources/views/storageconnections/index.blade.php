<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('StorageConnection') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-message :message="session('success')" type="success" />
            <x-sg-table type="basic"  id="storageconnectionDatatable">
                <x-sg-thead>
                    <tr>
                        <th>{{ __('SL') }}</th>
                            						<th>{{ __('Title') }}</th>
						<th>{{ __('Storagetype Title') }}</th>
						<th>{{ __('Target') }}</th>
						<th>{{ __('Storage') }}</th>
						<th>{{ __('Client') }}</th>
						<th>{{ __('Encription') }}</th>
						<th>{{ __('File Path') }}</th>

                        <th>{{ __('Actions' )}}</th>
                    </tr>
                </x-sg-thead>
                <x-sg-tbody>
                    @foreach ($storageconnections as $storageconnection)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $storageconnection->title }}</td>
						<td>{{ $storageconnection->storagetype_title }}</td>
						<td>{{ $storageconnection->target }}</td>
						<td>{{ $storageconnection->storage }}</td>
						<td>{{ $storageconnection->client }}</td>
						<td>{{ $storageconnection->encription }}</td>
						<td>{{ $storageconnection->file_path }}</td>

                        <td>
                            <x-sg-link-show href="{{route('storageconnections.show', $storageconnection->uuid)}}" />
                            {{-- <-sg-link-tree href="{{route('inputSpreadSheet')}}" /> --}}
                            <a class="btn btn-sm bg-green-800 border-2 border-green-800 btn-icon rounded-round legitRipple shadow mr-1" 
                            href="{{route('storageconnection', ['uuid' => $storageconnection->uuid])}}">
                            <i class="icon-file-spreadsheet2"></i></a>
                            <!-- <a class="btn btn-sm border-2 border-warning text-warning btn-icon rounded-round legitRipple shadow mr-1" href="route('inputSpreadSheet')}}"><i class="icon-file-spreadsheet2"></i></a> -->
                            <x-sg-link-edit href="{{route('storageconnections.edit', $storageconnection->uuid)}}" />
                            <x-sg-btn-delete action="{{route('storageconnections.destroy', $storageconnection->uuid)}}" method="post" />
                            <x-sg-link-history uuid="{{ $storageconnection->uuid }}"  api="/api/storageconnection-histories-list" />
                        </td>
                    </tr>
                    @endforeach
                </x-sg-tbody>
            </x-sg-table>
        </x-slot>
        <x-slot name="cardFooterCenter">

            <x-sg-link-create href="{{route('storageconnections.create')}}" />

        </x-slot>
    </x-sg-card>



@push('js')


<script>
   $(document).ready(function() {
        $('#storageconnectionDatatable').DataTable({
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="icon-grid3"></i>',
                    className: 'btn bg-indigo-400 btn-icon dropdown-toggle'
                }
            ],
            stateSave: false,
            columnDefs: [
                {
                    targets: 0,
                    visible: true
                }
            ]
        });
    });
</script>
@endpush

</x-sg-master>
