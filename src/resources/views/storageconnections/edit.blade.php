<x-sg-master>
    <x-sg-card>
        <x-slot name="heading">
            {{ __('StorageConnection') }}
        </x-slot>
        <x-slot name="body">
            <x-sg-alert-errors :errors="$errors" />
            <form action="{{ route('storageconnections.update', $storageconnection->uuid) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="row">
                    {{--relationalFields--}}
                    <!-- title -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="titleInput">{{ __('Title') }}</x-sg-label>
        <x-sg-text type="text" id="titleInput" name="title" :value="old('title', $storageconnection->title)" placeholder="" />
        <x-sg-alert-errors name="title" />
    </div>
</div>


<!-- storagetype_title -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="storagetype_titleInput">{{ __('Storagetype Title') }}</x-sg-label>
        <x-sg-selecttrio connection="keepmealone" tableName="storagetypes" name="storagetype" :selected="old('storagetype_id', $storageconnection->storagetype_id)"  />
        {{-- <sg-text type="text" id="storagetype_titleInput" name="storagetype_title" :value="old('storagetype_title', $storageconnection->storagetype_title)" placeholder="" /> --}}
        <x-sg-alert-errors name="storagetype_title" />
    </div>
</div>

<!-- target -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="targetInput">{{ __('Target') }}</x-sg-label>
        <x-sg-text type="text" id="targetInput" name="target" :value="old('target', $storageconnection->target)" placeholder="" />
        <x-sg-alert-errors name="target" />
    </div>
</div>

<!-- storage -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="storageInput">{{ __('Storage') }}</x-sg-label>
        <x-sg-text type="text" id="storageInput" name="storage" :value="old('storage', $storageconnection->storage)" placeholder="" />
        <x-sg-alert-errors name="storage" />
    </div>
</div>

<!-- client -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="clientInput">{{ __('Client') }}</x-sg-label>
        <x-sg-text type="text" id="clientInput" name="client" :value="old('client', $storageconnection->client)" placeholder="" />
        <x-sg-alert-errors name="client" />
    </div>
</div>

<!-- secret -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="secretInput">{{ __('Secret') }}</x-sg-label>
        <x-sg-text type="text" id="secretInput" name="secret" :value="old('secret', $storageconnection->secret)" placeholder="" />
        <x-sg-alert-errors name="secret" />
    </div>
</div>

<!-- encription -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="encriptionInput">{{ __('Encription') }}</x-sg-label>
        <x-sg-text type="text" id="encriptionInput" name="encription" :value="old('encription', $storageconnection->encription)" placeholder="" />
        <x-sg-alert-errors name="encription" />
    </div>
</div>

<!-- file_path -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="file_pathInput">{{ __('File Path') }}</x-sg-label>
        <x-sg-text type="text" id="file_pathInput" name="file_path" :value="old('file_path', $storageconnection->file_path)" placeholder="" />
        <x-sg-alert-errors name="file_path" />
    </div>
</div>

<!-- file_content -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="file_contentInput">{{ __('File Content') }}</x-sg-label>
        <x-sg-text type="text" id="file_contentInput" name="file_content" :value="old('file_content', $storageconnection->file_content)" placeholder="" />
        <x-sg-alert-errors name="file_content" />
    </div>
</div>

<!-- scope -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="scopeInput">{{ __('Scope') }}</x-sg-label>
        <x-sg-text type="text" id="scopeInput" name="scope" :value="old('scope', $storageconnection->scope)" placeholder="" />
        <x-sg-alert-errors name="scope" />
    </div>
</div>

<!-- data_connection -->
<div class="{{$decoration['class']['elementwrapper']}}" >
    <div class="{{$decoration['class']['elementcontainer'] }}" >
        <x-sg-label for="data_connectionInput">{{ __('Data Connection') }}</x-sg-label>
        <x-sg-text type="text" id="data_connectionInput" name="data_connection" :value="old('data_connection', $storageconnection->data_connection)" placeholder="" />
        <x-sg-alert-errors name="data_connection" />
    </div>
</div>



                    <div class = "{{$decoration['class']['formfooter']}}" >
                        <x-sg-btn-submit />
                        <x-sg-btn-cancel />
                    </div>
                </div>
            </form>
        </x-slot>
        <x-slot name="cardFooterCenter">
            <x-sg-link-list href="{{route('storageconnections.index')}}" />
        </x-slot>
    </x-sg-card>
@push('css')
{{--pagespecific-css--}}
@endpush

@push('js')
{{--pagespecific-js--}}
@endpush

</x-sg-master>
