<?php

namespace Genie\Schematojson\Schema\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UserTrackable;


class StructureHistory extends Model
{
    protected $table = 'json_structure_histories';
    protected $guarded = ['id'];

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
