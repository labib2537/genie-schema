<?php

namespace Genie\Schematojson\Schema\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UserTrackable;


class StoragetypeHistory extends Model
{
    protected $table = 'storagetype_histories';
    protected $guarded = ['id'];

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
