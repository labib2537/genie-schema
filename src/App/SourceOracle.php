<?php

namespace Genie\Schematojson\Schema\App;

use Genie\Schematojson\Schema\Contracts\IConnect;
use Illuminate\Support\Facades\DB;
use Genie\Schematojson\Schema\Models\Table;

class SourceOracle implements IConnect
{
    public function connect($connection, $uuid)
    {
        $schema = DB::connection($connection)->getDoctrineSchemaManager();
        $tables = $schema->listTableNames();
        // dd($tables);
        $columns = [];
        foreach($tables as $key => $table){
            $columns[] =$schema->listTableColumns($tables[$key]);
        }
        // dd($tables[0]);
        $columns = $schema->listTableColumns($tables[0]);
        // dd($columns);
        
        return view('schema::tables.index', compact('tables', 'connection', 'uuid'));

        
        
            
        //     $connection = 'product';
        //     $tables = $this->tables($connection);
            // return view('schema::connect.oracle')->with($tables);
    }

    public function tables($connection){
        // return view('schema::tables.index', compact('tables'));
        $tables = DB::connection($connection)->table('items')->limit(10)->get();
        dd($tables);
    }

    public function columns($table){
        
    }

    public function constraints($column){
        
    }



}
