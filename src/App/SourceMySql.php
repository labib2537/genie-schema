<?php

namespace Genie\Schematojson\Schema\App;
use Illuminate\Support\Facades\DB;

use Genie\Schematojson\Schema\Contracts\IConnect;


class SourceMySql implements IConnect
{
    public function connect($connection, $uuid)
    {
        // dd($connection);
        $schema = DB::connection($connection)->getDoctrineSchemaManager();
        // dd($schema);
        $tables = $schema->listTableNames();
        // dd($tables);
        $columns = [];
        foreach($tables as $key => $table){
            $columns[] =$schema->listTableColumns($tables[$key]);
        }
        // dd($tables[0]);
        $columns = $schema->listTableColumns($tables[0]);
        // dd($columns);
        
        return view('schema::tables.index', compact('tables', 'connection', 'uuid'));
        // return view('schema::connectsheets.mysql');
    }


}
