<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJsonStructureHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('keepmealone')->create('json_structure_histories', function (Blueprint $table) {
            $table->increments('id');
            ##ELOQUENTRELATIONSHIPCOLUMNS##
            $table->unsignedBigInteger('structure_id')->nullable();
            
			$table->uuid('uuid');
			$table->string('column', '64')->nullable();
			$table->string('data_type', '32')->nullable();
			$table->string('lenght')->nullable();
			$table->string('collation', '128')->nullable();
			$table->string('attributes', '64')->nullable();
			$table->string('null')->nullable();
			$table->string('default', '32')->nullable();
			$table->string('comment', '128')->nullable();
			$table->string('extra', '128')->nullable();
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->softDeletes();
			$table->string('action', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('keepmealone')->dropIfExists('json_structure_histories');
    }
}