<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoragetypeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('keepmealone')->create('storagetype_histories', function (Blueprint $table) {
            $table->increments('id');
            ##ELOQUENTRELATIONSHIPCOLUMNS##
            $table->unsignedBigInteger('storagetype_id')->nullable();
            
			$table->uuid('uuid');
			$table->string('title', '16');
			$table->string('type', '16')->nullable()->comments('dropdown from array');
			$table->string('description')->nullable();
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->softDeletes();
			$table->string('action', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('keepmealone')->dropIfExists('storagetype_histories');
    }
}