<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStorageconnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('keepmealone')->create('storageconnections', function (Blueprint $table) {
            $table->increments('id');
            ##ELOQUENTRELATIONSHIPCOLUMNS##
            
            
			$table->uuid('uuid');
			$table->string('title');
			$table->bigInteger('storagetype_id')->nullable();
			$table->string('storagetype_uuid', '36')->nullable();
			$table->string('storagetype_title', '64')->nullable();
			$table->string('target', '64')->nullable()->comments('host - IP/domain');
			$table->string('storage', '64')->nullable()->comments('database - dbname/saas platform');
			$table->string('client', '64')->nullable()->comments('dbuser/client_id');
			$table->string('secret', '128')->nullable()->comments('dbpass/secret/API key');
			$table->string('encription', '64')->nullable();
			$table->string('file_path', '255')->nullable();
			$table->text('file_content')->nullable();
			$table->string('scope', '128')->nullable();
			$table->json('data_connection')->nullable();
			$table->string('qrcode', '255')->nullable();
			$table->unsignedBigInteger('created_by')->nullable();
			$table->unsignedBigInteger('updated_by')->nullable();
			$table->unsignedBigInteger('deleted_by')->nullable();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('keepmealone')->dropIfExists('storageconnections');
    }
}